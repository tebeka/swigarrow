package swigarrow

import (
	"fmt"

	"swigarrow/internal"
)

type DType uint32

var (
	Integer64DType = DType(internal.GetInteger64Type())
	Float64DType   = DType(internal.GetFloat64Type())
	StringDType    = DType(internal.GetStringType())
	TimestampDType = DType(internal.GetTimestampType())
)

type Int64Builder struct {
	bld internal.Int64Builder
}

func NewInt64Builder() *Int64Builder {
	ptr := internal.New_int64_builder()
	bld := internal.NewInt64Builder(ptr)
	return &Int64Builder{bld: bld}
}

func (b *Int64Builder) Append(i int64) error {
	if b.bld == nil {
		return fmt.Errorf("not Int64 builder")
	}
	err := b.bld.Append(i)
	if err != "" {
		return fmt.Errorf(err)
	}
	return nil
}

type Float64Builder struct {
	bld internal.Float64Builder
}

func NewFloat64Builder() *Float64Builder {
	ptr := internal.New_float64_builder()
	bld := internal.NewFloat64Builder(ptr)
	return &Float64Builder{bld: bld}
}

func (b *Float64Builder) Append(i float64) error {
	if b.bld == nil {
		return fmt.Errorf("not Int64 builder")
	}
	err := b.bld.Append(i)
	if err != "" {
		return fmt.Errorf(err)
	}
	return nil
}
