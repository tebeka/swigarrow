package swigarrow

import (
	"testing"
)

func TestInt64Builder(t *testing.T) {
	bld := NewInt64Builder()
	for i := int64(0); i < 117; i++ {
		bld.Append(i)
	}
}

func TestFloat64Builder(t *testing.T) {
	bld := NewFloat64Builder()
	for i := float64(0); i < 117; i++ {
		bld.Append(i)
	}
}
