go: all
	cp internal/internal.go /tmp
	$(MAKE) clean

all:
	cd internal && swig \
	    -I/opt/miniconda/include \
	    -c++ \
	    -go -intgosize 64 \
	    internal.swigcxx

clean:
	rm -f internal/internal.go
	rm -f internal/internal_wrap.cxx

fresh: clean all
