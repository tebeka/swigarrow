#include <arrow/api.h>

const int Integer64Type = arrow::Type::INT64;
const int Float64Type = arrow::Type::DOUBLE;
const int StringType = arrow::Type::STRING;
const int TimestampType = arrow::Type::TIMESTAMP;

union Any {
    char *s;
    double f;
    int64_t i;
    void *ptr;
};

struct Result {
    char *err;
    Any val;
};

template<class B, class T>
class Builder {
public:
    Builder(B *ptr) {
        this->ptr = ptr;
    }

    const char *Append(T value) {
        arrow::Status status = this->ptr->Append(value);
        if (!status.ok()) {
            return status.message().c_str();
        } 

        return nullptr;
    }

    Result Finish() {
        Result res;
        std::shared_ptr<arrow::Array> array;
        auto status = ptr->Finish(&array);
        if (!status.ok()) {
            res.err = (char *)status.message().c_str();
        } else {
            res.val.ptr = array.get();
        }
        return res;
    }
private:
    B *ptr;
};

arrow::Int64Builder *new_int64_builder() {
    return new arrow::Int64Builder();
}

arrow::DoubleBuilder *new_float64_builder() {
    return new arrow::DoubleBuilder();
}