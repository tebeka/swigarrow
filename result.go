package swigarrow

import (
	"errors"

	"swigarrow/internal"
)

type result struct {
	err error
	val internal.Any
}

func newResult(ir internal.Result) *result {
	r := result{val: ir.GetVal()}
	if s := ir.GetErr(); s != "" {
		r.err = errors.New(s)
	}
	return &r
}

func (r *result) Err() error {
	return r.err
}

func (r *result) Str() string {
	return r.val.GetS()
}

func (r *result) Float() float64 {
	return r.val.GetF()
}

func (r *result) Int() int64 {
	return r.val.GetI()
}

func (r *result) Ptr() uintptr {
	return r.val.GetPtr()
}
